#Bacon tasting
A short and tasty introduction to functional reactive programming with bacon.js.

## Functional Reactive Programming - WAT?
Let's go through FRP starting from the end.

### Programming
Web and mobile apps nowadays tends to involve a multitude of user and data events. Think about Facebook for example, there's a live feed of the friends who are online, news feed, ads, chats, suggestions, applications and other stuff. All of these can be interacted in a myriad of ways. Then there's all the stuff happening under the hood, event buses, variables, properties, changing data structures and so on. Also most of this happens asynchronously.


![facebook.png](https://bitbucket.org/repo/Mq9xgR/images/2804966182-facebook.png)


One way to make sense of this is to think the application as a bunch of data streams, where the data stream can be a news feed, mouse click or other user event, but also a variable inside the program that changes over time.

### Reactive Programming
Reactive programming gives us nice high level abstractions to play with these data streams: observable event streams and properties. Event streams are for thinks like mouse clicks that don't have a current value, properties are for thinks like mouse x/y coordinates that have a value.

![event stream.png](https://bitbucket.org/repo/Mq9xgR/images/945916426-event%20stream.png)

### Functional Reactive Programming
Finally we add the functional part: you get an amazing toolbox of functions to combine, create and filter any of those streams. Functions observe the event streams, asynchronously triggering when there is a new value in the stream.

## Functional Reactive Programming - Why?

Reactive programming raises the level of abstraction of your code so you can focus on the interdependence of events that define the business logic, rather than having to constantly fiddle with a large amount of implementation details.

## Bacon.js
![logo.png](https://bitbucket.org/repo/Mq9xgR/images/3564885687-logo.png)


Bacon.js is a FRP library for javascript. Healthy amount of activity, ready for production use, MIT license. Around 10kB minified and zipped.
https://baconjs.github.io/index.html


There’s also alternatives like RxJS from Microsoft. The core concepts are the same, with some differences in semantics.


You can find FRP libraries for a wide variety of platforms. Note that they are libraries not frameworks, so it’s easy to use them together with other tools. Think underscore vs. angular.js. Once you get the hang of one of the libraries it's pretty smooth to switch to another.

## Getting started with the tutorial
Note that you need npm (node package manager) to install the dependencies.

https://www.npmjs.com/

Start by cloning or downloading the repo.

After that

> cd webapp

> npm install

open webapp/index.html

check out webapp/app/app.js for instructions and TODOs

## Further reading
Great intro by Andre Staltz, this one uses RxJS though.

https://gist.github.com/staltz/868e7e9bc2a7b8c1f754