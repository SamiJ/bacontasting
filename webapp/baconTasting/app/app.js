/**
 *
 * Welcome! In this session we are writing a small form that helps online shoppers calculate monthly
 * fee for a big purchase.
 *
 * First open index.html and take a look on what is already provided.
 *
 * You'll want to have the bacon api open in your favourite browser when doing this
 * https://baconjs.github.io/api.html
 *
 * Note that there are some extra steps in the example code for clarity and learning purposes.
 *
 * TODO Add support for interest rate
 * Hints: Bacon.combineWith(), getTotalInterest function below.
 * Bonus: add rounding to two decimals 13.33333333 -> 13.33
 *
 *
 * TODO if time: Enable/Disable buy button based on price and other validations, for example monthly fee must be above 50
 * Hints: .map() values to boolean. Create valid properties .and() make sure all validations pass.
 *
 *
 * TODO advanced homework
 * Turn monthly fee into an input field, so users can also check what they can get with the monthly payment
 * they can afford, or how long payment plan they need.
 * Hint: https://baconjs.github.io/api.html#join-patterns-and-properties
 *
 */

define(function (require) {
// libraries
    var $ = require('jquery')
    var _ = require('underscore')
    var Bacon = require('bacon')

// dev stuff
    $.fn.asEventStream = Bacon.$.asEventStream //NOTE: explicit monkey patching here so loading order doesn't matter
    var debug = true
    var LOG = debug ? console.log.bind(console) : function () {}

// vars

// jquery
    var $price = $('#price')
    var $monthlyFee = $('#monthlyFee')
    var $monthlyPayments = $('input:radio[name="monthly"]')
    var $interestRate = $('#interestRate')
    var $totalCost = $('#totalCost')
    var $buy = $('#buy')

// event streams
    var priceStream = $price
        .asEventStream('input', getValue)

    var priceProperty = priceStream
        .toProperty($price.val())

    var monthlyPaymentsStream = $monthlyPayments
        .asEventStream('change', getValue)

    var monthlyPaymentsProperty = monthlyPaymentsStream
        .toProperty($monthlyPayments.val())

    // TODO: Note how log() below inputs the value of the observable to console, it's handy when developing but you might want to remove it later
    var monthlyFeeProperty = Bacon
        .combineWith(divide, priceProperty, monthlyPaymentsProperty)
        .log('Monthly fee')

    // TODO Now it's your turn to serve some juicy bacon!
    // Hint: Check how streams and properties are created above and create the interestRate observables.
    // You can use .log() to make sure you got it right.
    var interestRateStream
    var interestRateProperty

    // TODO look above how to combine multiple properties, note the provided getTotalInterest function.
    var totalInterestProperty

    //TODO divide is provided already
    var interestPerMonthProperty

    // TODO you may want to create a sum function or just use anonymous one here
    var totalMonthlyFeeProperty

    // TODO lets calculate the total cost as well.
    var totalCostProperty

    // TODO validations stuff starts here, make sure you got the interest rate right first.
    // Hint: .map to boolean might be handy here. You might want to write things like interestRateValidProperty
    // You can start with a min monthly fee of 100 or some other validation.
    var buyEnabledProperty

// Event Stream handlers - this is where we cause side effects, i.e. manipulate dom based on events

    // TODO: you may want to change this after switching to total cost with interest
    // This calls the text function of $monthlyFee with the value of the property every time there's a new
    // value in monthlyFee. You could also give function(value) {//handler} as parameter.
    monthlyFeeProperty
            .onValue($monthlyFee, 'text')

// Functions
    function getValue(event) {
        return $(event.target).val()
    }

    function divide(x,y) {
        return x / y
    }

    // NOTE: naive implementation, could use proper compound interest rate here
    // maybe add http://financejs.org/ support at some point
    function getTotalInterest(sum, interestRate, timeInMonths) {
        return sum * interestRate/100 * timeInMonths/12
    }

    function init() {
        $.noop()
    }

    return {init: init}
})
